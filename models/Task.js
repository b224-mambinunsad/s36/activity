// Create the Schema and model and exports the file.
const mongoose = require("mongoose");


const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
	 	default: "pending"
	}
});



module.exports = mongoose.model("Task", taskSchema);